<%-- 
    Document   : theo_doi_diem
    Created on : Feb 26, 2020, 2:27:45 PM
    Author     : NguyenDuc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Follow By Semester</title>
        <script src="js/vue.js"></script>
        <link rel="stylesheet" type="text/css" href="css/followScore.css">
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    </head>
    <body>
        <!--sidenav-->
        <div class="sidenav" id="mySidenav">
            <div class="item item-without-submenu">
                <form action="<c:url value="/User"/>" method="GET">
                    <input type="submit" value="Cập nhật user">
                </form>
            </div>
            <div class="item open-sub-menu">
                <div class="main-item" v-on:click="showSubMenu">Theo dõi điểm</div>
                <div class="sub-menu" v-bind:style="style">
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySubject"/>" method="GET">
                            <input type="submit" value="Theo dõi theo môn">
                        </form>
                    </div>
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySemester" />" method="GET">
                            <input type="submit" value="Theo dõi theo kì">
                        </form>
                    </div>
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySemesterChart" />">
                            <input type="submit" value="Thống kê điểm">
                        </form>
                    </div>
                </div>
            </div>
            <div class="item item-without-submenu">
                <form action="<c:url value="/SetupConfig"/>" method="GET">
                    <input type="submit" value="Cấu hình điểm">
                </form>
            </div>
            <a class="closeBtn" onclick="closeSidenav()">&times;</a>
            <img src="image/unnamed.png" class="img" alt="logo ptit" width="50" height="50">
            <a class="logo-name" href="image/">QUẢN LÝ ĐIỂM</a>
            <a class="returnBtn" href="#" onclick="openSidenav()">></a>
        </div>
        <!--end sidenav-->
        <!--topnav-->
        <div class="topnav">
            <div class="topnav-content">
                <div class="topnav-item">Trang chủ</div>
                <div class="topnav-item">Thông tin</div>
                <div class="topnav-item">
                    NguyenDuc <img style="position: absolute; top: 12px; margin-left: 19px"src="image/user.png" alt="user avatar" width="30" height="30">
                </div>
            </div>
        </div>
        <!--end topnav-->
        <!--content-->
        <div class="content">
            <h3>Theo dõi điểm theo kì học</h3>
            <form action="<c:url value="/FollowBySemester" />" method="POST" class="form-search">
                <input style="display: inline-block; margin-right: 15px;" type="text" class="text-input" name="studentCode" placeholder="Nhập mã sinh viên" value="${studentCode}">

                <div style="display: inline-block">
                    <div class="drop-down-semester" style="margin-left: 0;" v-on:click="showDropDown = !showDropDown;">
                        {{selectedSemester}}
                        <div class="semester" v-if="showDropDown">
                            <a v-for="lm in listSemester" v-on:click="selectedSemester=lm" name="semester"> {{lm}} </a>
                        </div>
                        <input type="hidden" name="semester" v-model="selectedSemester">
                    </div>
                </div>

                <div style="display: inline-block;">
                    <div class="drop-down-year" v-on:click="showDropDown = !showDropDown;">
                        {{selectedYear}}
                        <div class="year" v-if="showDropDown">
                            <c:forEach var="year" items="${listYear}">
                                <a v-on:click="selectedYear=${year.startYear}+' - '+${year.endYear}">
                                    ${year.startYear} - ${year.endYear}
                                </a>
                            </c:forEach>
                        </div>
                        <input type="hidden" name="year" v-model="selectedYear">
                    </div>
                </div>

                <div style="display: inline-block;" class="v-drop-down">
                    <div class="drop-down" v-on:click="showDropDown = !showDropDown;">
                        {{selectedItem}}
                        <div class="drop-down-item" v-if="showDropDown">
                            <a v-for="lm in listItem" v-on:click="selectedItem=lm" name="item"> {{lm}} </a>
                        </div>
                        <input type="hidden" name="optionSelected" v-model="selectedItem">
                    </div>
                </div>

                <input class="search-btn" type="submit" value="Tìm kiếm">
                <p class="err-msg">${msgErr}</p>
            </form>
            <c:forEach var="semester" items="${semesterStatics}">
                <p style="font-weight: bold;">Thông tin ${semester.get(0).semesterName} năm học ${semester.get(0).startYear} - ${semester.get(0).endYear}</p>
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <th width="5%">STT</th>
                        <th width="10%">Mã SV</th>
                        <th width="15%">Tên môn học</th>
                        <th width="10%">Số tín chỉ</th>
                        <th width="10%">Điểm tp 1</th>
                        <th width="10%">Điểm tp 2</th>
                        <th width="10%">Điểm tp 3</th>
                        <th width="10%">Điểm cuối kì</th>
                        <th width="10%">Điểm trung bình</th>
                        <th width="10%">Điểm bằng chữ</th>
                    </tr>
                    <c:forEach var="subject" items="${semester}">
                        <tr>
                            <td>${subject.stt}</td>
                            <td>${subject.studentCode}</td>
                            <td>${subject.subjectName}</td>
                            <td>${subject.numCredit}</td>
                            <td>${subject.score1}</td>
                            <td>${subject.score2}</td>
                            <td>${subject.score3}</td>
                            <td>${subject.finalScore}</td>
                            <td>${subject.tb}</td>
                            <td>${subject.letter}</td>
                        </tr>
                    </c:forEach>
                </table>
                <br>
            </c:forEach>
        </div>

    </body>
    <script>
        function closeSidenav() {
            var distance = '60px';
            document.getElementsByClassName("sidenav")[0].style.width = distance;
            document.getElementsByClassName("closeBtn")[0].style.display = 'none';
            document.getElementsByClassName("returnBtn")[0].style.display = 'block';
            document.getElementsByClassName("topnav")[0].style.left = distance;
            document.getElementsByClassName("content")[0].style.left = distance;
            document.getElementsByClassName("logo-name")[0].style.display = 'none';
        }

        function openSidenav() {
            var distance = '200px';
            var op = document.getElementsByClassName("sidenav")[0];
            op.style.width = distance;
            document.getElementsByClassName("closeBtn")[0].style.display = 'block';
            document.getElementsByClassName("returnBtn")[0].style.display = 'none';
            document.getElementsByClassName("topnav")[0].style.left = distance;
            document.getElementsByClassName("content")[0].style.left = distance;
            document.getElementsByClassName("logo-name")[0].style.display = 'inline';
        }

        function colourize() {
            var dnl = document.getElementsByTagName("tr");
            for (i = 0; i < dnl.length; i++) {
                if ((Math.round(i / 2) * 2) === ((i / 2) * 2))
                    dnl.item(i).style.background = "#F0F0F0";
            }
        }
        window.onload = colourize;

        function showErrMsg(msg) {
            console.log("msg:", mgs);
            if (msg !== "")
                alert(msg);
        }

        var openSubMenu = new Vue({
            el: '.open-sub-menu',
            data: {
                show: false,
                style: '',
            },
            methods: {
                showSubMenu: function () {
                    this.show = !this.show;
                    if (this.show === false) {
                        this.style = 'height: 0'
                    } else
                        this.style = 'height: 120px'
                }
            }
        });

        var dropDownMonth = new Vue({
            el: '.drop-down-month',
            data: {
                listMonth: [
                    "tháng 1",
                    "tháng 2",
                    "tháng 3",
                    "tháng 4",
                    "tháng 5",
                    "tháng 6",
                    "tháng 7",
                    "tháng 8",
                    "tháng 9",
                    "tháng 10",
                    "tháng 11",
                    "tháng 12",
                ],
                selectedMonth: 'Chọn tháng',
                showDropDown: false
            }
        });

        var dropDownYear = new Vue({
            el: '.drop-down-year',
            data: {
                selectedYear: "Chọn năm học",
                showDropDown: false
            }
        });

        var dropDownSemester = new Vue({
            el: '.drop-down-semester',
            data: {
                listSemester: ["Kì 1", "Kì 2", "Kì hè"],
                selectedSemester: "Chọn kì học",
                showDropDown: false
            }
        });

        var dropDown = new Vue({
            el: '.v-drop-down',
            data: {
                listItem: ["Xem tất cả các kì", "Xem kì đã chọn"],
                selectedItem: "Chọn cách xem",
                showDropDown: false
            }
        });
    </script>
</html>