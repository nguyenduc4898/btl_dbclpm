<%-- 
    Document   : theo_doi_diem
    Created on : Feb 26, 2020, 2:27:45 PM
    Author     : NguyenDuc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Follow By Subject</title>
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <link rel="stylesheet" type="text/css" href="css/followScore.css">
        <link rel="stylesheet" type="text/css" href="css/user.css">
        <link rel="stylesheet" type="text/css" href="css/configSetup.css">
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    </head>
    <body>
        <!--sidenav-->
        <div class="sidenav" id="mySidenav">
            <div class="item item-without-submenu">
                <form action="<c:url value="/User"/>" method="GET">
                    <input type="submit" value="Cập nhật user">
                </form>
            </div>
            <div class="item open-sub-menu">
                <div class="main-item" v-on:click="showSubMenu">Theo dõi điểm</div>
                <div class="sub-menu" v-bind:style="style">
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySubject"/>" method="GET">
                            <input type="submit" value="Theo dõi theo môn">
                        </form>
                    </div>
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySemester" />" method="GET">
                            <input type="submit" value="Theo dõi theo kì">
                        </form>
                    </div>
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySemesterChart" />">
                            <input type="submit" value="Thống kê điểm">
                        </form>
                    </div>
                </div>
            </div>
            <div class="item item-without-submenu">
                <form action="<c:url value="/SetupConfig"/>" method="GET">
                    <input type="submit" value="Cấu hình điểm">
                </form>
            </div>
            <a class="closeBtn" onclick="closeSidenav()">&times;</a>
            <img src="image/unnamed.png" class="img" alt="logo ptit" width="50" height="50">
            <a class="logo-name" href="image/">QUẢN LÝ ĐIỂM</a>
            <a class="returnBtn" href="#" onclick="openSidenav()">></a>
        </div>
        <!--end sidenav -->
        <!--topnav-->
        <div class="topnav">
            <div class="topnav-content">
                <div class="topnav-item">Trang chủ</div>
                <div class="topnav-item">Thông tin</div>
                <div class="topnav-item">
                    NguyenDuc <img style="position: absolute; top: 12px; margin-left: 19px"src="image/user.png" alt="user avatar" width="30" height="30">
                </div>
            </div>
        </div>
        <!--end topnav -->

        <!--content-->
        <div class="content" style="z-index: 0;">
            <h3>CÀI ĐẶT CẤU HÌNH</h3>
            <div class="list-subject-iframe">
                <div>
                    <a class="closeBtn" style="color: black; top: 0; right: 30px; font-size: 30px" onclick="showPopup(0)">&times;</a>
                </div>
                <div>
                    <iframe frameBorder="0" src="public/list_subject.html" height="500px" width="500px"></iframe>
                </div>
            </div>
            <div class="mt-4">
                <form action="<c:url value="/SetupConfig" />" method="POST" name="formConfig" onsubmit="return validateConfig()">
                    <div style="display: inline-block">
                        <input type="text" class="text-input" name="subjectName" placeholder=" Nhập tên môn học">
                        <a style="margin-left: 15px; color: #0099ff; cursor: pointer" onclick="showPopup(1)">Tra cứu tên môn học</a>
                    </div>
                    <p id="msg-update" style="position: absolute; top: 110px; left: 820px; visibility: visible; color: ${colorMsg}; font-weight: 600;">${msgUpdate}</p>
                    <table class="mt-4">
                        <tr>
                            <th colspan="4">Tên môn học: ${subjectName}</th>
                        <tr>
                        <tr>
                            <td width="25%" style="text-align: center">Điểm thành phần 1</td>
                            <td width="25%" style="text-align: center">Điểm thành phần 2</td>
                            <td width="25%" style="text-align: center">Điểm thành phần 3</td>
                            <td width="25%" style="text-align: center">Điểm cuối kỳ</td>
                        </tr>
                        <tr>
                            <td style="text-align: center;">
                                <input class="input-config" type="text" id="tp1" name="tp1" value="${tp1}">
                            </td>
                            <td style="text-align: center;">
                                <input class="input-config" type="text" id="tp2" name="tp2" value="${tp2}">
                            </td>
                            <td style="text-align: center;">
                                <input class="input-config" type="text" id="tp3" name="tp3" value="${tp3}">
                            </td>
                            <td style="text-align: center;">
                                <input class="input-config" type="text" id="final" name="final" value="${final}">
                            </td>
                        </tr>
                    </table>
                    <div style="width: 1000px; text-align: center">
                        <input type="submit" class="mt-4 btn-user" value="Cập nhật thành phần điểm">
                    </div>
                </form>
            </div>
        </div>
    </body>
    <script>
        window.addEventListener("message", function (event) {
            console.log("Nhận d-mail: ", event.data);
            if (event.data.subjectName)
                document.getElementsByClassName("text-input")[0].value = event.data.subjectName;
        });

        function closeSidenav() {
            var distance = '60px';
            document.getElementsByClassName("sidenav")[0].style.width = distance;
            document.getElementsByClassName("closeBtn")[0].style.display = 'none';
            document.getElementsByClassName("returnBtn")[0].style.display = 'block';
//            document.getElementsByClassName("topnav")[0].style.left = distance;
            document.getElementsByClassName("content")[0].style.left = distance;
            document.getElementsByClassName("logo-name")[0].style.display = 'none';
        }

        function openSidenav() {
            var distance = '200px';
            var op = document.getElementsByClassName("sidenav")[0];
            op.style.width = distance;
            document.getElementsByClassName("closeBtn")[0].style.display = 'block';
            document.getElementsByClassName("returnBtn")[0].style.display = 'none';
//            document.getElementsByClassName("topnav")[0].style.left = distance;
            document.getElementsByClassName("content")[0].style.left = distance;
            document.getElementsByClassName("logo-name")[0].style.display = 'inline';
        }

        function colourize() {
            var dnl = document.getElementsByTagName("tr");
            for (i = 0; i < dnl.length; i++) {
                if ((Math.round(i / 2) * 2) === ((i / 2) * 2))
                    dnl.item(i).style.background = "#F0F0F0";
            }
        }

        window.onload = colourize;

        function showErrMsg(msg) {
            console.log("msg:", mgs);
            if (msg !== "")
                alert(msg);
        }

        function showPopup(x) {
            if (x === 1)
                document.getElementsByClassName("list-subject-iframe")[0].style.display = 'block';
            if (x === 0)
                document.getElementsByClassName("list-subject-iframe")[0].style.display = 'none';
        }

        setInterval(function () {
            var value = document.getElementById("msg-update").value;
            if (value != "") {
                console.log("vào đây");
                setTimeout(function () {
                    document.getElementById("msg-update").innerHTML = "";
                }, 2000);
            }
        }, 50);

        function validateConfig() {
            console.log("-------------------------------");
            var subjectName = document.forms['formConfig']['subjectName'].value;
            var tp1 = document.forms['formConfig']['tp1'].value;
            var tp2 = document.forms['formConfig']['tp2'].value;
            var tp3 = document.forms['formConfig']['tp3'].value;
            var fi = document.forms['formConfig']['final'].value;
            
            if (subjectName === ""){
                alert("Chưa nhập tên môn học");
                return false;
            }
            if (tp1 === "" || !Number.isInteger(parseInt(tp1))) {
                alert("Chưa nhập Điểm thành phần 1 hoặc nhập sai định dạng số");
                return false;
            }
            if (tp2 === "" || !Number.isInteger(parseInt(tp2))) {
                alert("Chưa nhập Điểm thành phần 2 hoặc nhập sai định dạng số");
                return false;
            }
            if (tp3 === "" || !Number.isInteger(parseInt(tp3))) {
                alert("Chưa nhập Điểm thành phần 3 hoặc nhập sai định dạng số");
                return false;
            }
            if (fi === "" || !Number.isInteger(parseInt(fi))) {
                alert("Chưa nhập Điểm cuối kỳ hoặc nhập sai định dạng số");
                return false;
            }
            if (parseInt(tp1) + parseInt(tp2) + parseInt(tp3) + parseInt(fi) !== 100){
                alert("Tổng các thành phần phải bằng 100");
                return false;
            }
            return true;
        }

        var openSubMenu = new Vue({
            el: '.open-sub-menu',
            data: {
                show: false,
                style: '',
            },
            methods: {
                showSubMenu: function () {
                    this.show = !this.show;
                    if (this.show === false) {
                        this.style = 'height: 0'
                    } else
                        this.style = 'height: auto'
                }
            }
        });
    </script>
</html>