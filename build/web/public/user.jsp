<%-- 
    Document   : theo_doi_diem
    Created on : Feb 26, 2020, 2:27:45 PM
    Author     : NguyenDuc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Follow By Subject</title>
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <link rel="stylesheet" type="text/css" href="css/followScore.css">
        <link rel="stylesheet" type="text/css" href="css/user.css">
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    </head>
    <body>
        <!--sidenav-->
        <div class="sidenav" id="mySidenav">
            <div class="item item-without-submenu">
                <form action="<c:url value="/User"/>" method="GET">
                    <input type="submit" value="Cập nhật user">
                </form>
            </div>
            <div class="item open-sub-menu">
                <div class="main-item" v-on:click="showSubMenu">Theo dõi điểm</div>
                <div class="sub-menu" v-bind:style="style">
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySubject"/>" method="GET">
                            <input type="submit" value="Theo dõi theo môn">
                        </form>
                    </div>
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySemester" />" method="GET">
                            <input type="submit" value="Theo dõi theo kì">
                        </form>
                    </div>
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySemesterChart" />">
                            <input type="submit" value="Thống kê điểm">
                        </form>
                    </div>
                </div>
            </div>
            <div class="item item-without-submenu">
                <form action="<c:url value="/SetupConfig"/>" method="GET">
                    <input type="submit" value="Cấu hình điểm">
                </form>
            </div>
            <a class="closeBtn" onclick="closeSidenav()">&times;</a>
            <img src="image/unnamed.png" class="img" alt="logo ptit" width="50" height="50">
            <a class="logo-name" href="image/">QUẢN LÝ ĐIỂM</a>
            <a class="returnBtn" href="#" onclick="openSidenav()">></a>
        </div>
        <!--end sidenav -->
        <!--topnav-->
        <div class="topnav">
            <div class="topnav-content">
                <div class="topnav-item">Trang chủ</div>
                <div class="topnav-item">
                    <form action="<c:url value="/LoginServlet"/>" method="GET">
                        <input type="submit" id="logout" value="Đăng xuất">
                    </form>
                </div>
                <div class="topnav-item">
                    NguyenDuc <img style="position: absolute; top: 12px; margin-left: 19px"src="image/user.png" alt="user avatar" width="30" height="30">
                </div>
            </div>
        </div>
        <!--end topnav -->

        <!--content-->
        <div class="content" style="z-index: 0;">
            <h3>QUẢN LÝ USER</h3>
            <div>
                <form action="<c:url value="/User"/>" method="POST">
                    <input type="text" class="text-input" placeholder="Nhập tên người dùng" name="nameSearch">
                    <input type="submit" class="search-user-btn ml-3" value="Tìm kiếm">
                    <input type="button" class="btn-user ml-3" value="Tạo người dùng mới" onclick="showPopupInsert()">
                </form>
            </div>
            <p id="msg-update" style="position: absolute; top: 110px; left: 765px; visibility: visible; color: ${colorMsg}; font-weight: 600;">${msgUpdate}</p>
            <div class="table-scrollable mt-5" v-if="${showTableUser}">
                <table border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <th width="5%">STT</th>
                        <th width="5%">id</th>
                        <th width="15%">Tên người dùng</th>
                        <th width="15%">Ngày sinh</th>
                        <th width="18%">Email</th>
                        <th width="12%">SĐT</th>
                        <th width="15%">Username</th>
                        <th width="10%">Vai trò</th>
                        <th width="6%" colspan="2">Cập nhật</th>
                    </tr>
                    <c:forEach var="user" items="${users}">
                        <tr>
                            <td>${user.stt}</td>
                            <td>${user.id}</td>
                            <td>${user.fullName}</td>
                            <td>${user.dateOfBirth}</td>
                            <td>${user.email}</td>
                            <td>${user.phone}</td>
                            <td>${user.userName}</td>
                            <td>${user.role}</td>
                            <td width="3%">
                                <input type="hidden" name="fullName" value="${user.fullName}">
                                <img src='image/edit-button.png' 
                                     width="15px" 
                                     height="15px" 
                                     class="img-edit"
                                     onclick="showPopupEdit({
                                                 'id': '${user.id}',
                                                 'fullName': '${user.fullName}',
                                                 'dateOfBirth': '${user.dateOfBirth}',
                                                 'email': '${user.email}',
                                                 'phone': '${user.phone}',
                                                 'userName': '${user.userName}',
                                                 'role': '${user.role}'
                                             })">
                            </td>
                            <td width="3%">
                                <img src='image/detele-button.png' 
                                     width="15px" 
                                     height="15px" 
                                     class="img-delete"
                                     onclick="showPopupDelete('${user.id}', '${user.userName}')">
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </div>
            <!-- Popup edit -->
            <div class="popup-edit">
                <form action="<c:url value="/UserUpdate" />" method="POST" name="formEdit" onsubmit="return validateFormEdit()">
                    <h4 style="text-align: center;">CẬP NHẬT THÔNG TIN USER</h4>
                    <p style="color: #ff3333; text-align: right; font-size: 12px;">Màu đỏ: không thể thay đổi</p>
                    <div class="width-1">
                        <span style="color: #ff3333">Id:</span>
                        <input type="text" class="text-input" name="_id" id="_id" readonly="readonly">
                    </div>
                    <div class="width-1">   
                        <span style="color: #ff3333">Username:</span>
                        <input type="text" class="text-input" name="userName" id="userName" readonly="readonly">
                    </div>
                    <div class="width-1">
                        <span style="color: #ff3333">Vai trò:</span>
                        <input type="text" class="text-input" name="role" id="role" readonly="readonly">
                    </div>
                    <div class="width-1">
                        <span>Tên người dùng:</span>
                        <input type="text" class="text-input" name="fullName" id="fullName">
                    </div>
                    <div class="width-1">
                        <span>Ngày sinh:</span>
                        <input type="text" class="text-input" name="dateOfBirth" id="dateOfBirth">
                    </div>
                    <div class="width-1">
                        <span>Email:</span>
                        <input type="text" class="text-input" name="email" id="email">
                    </div>
                    <div class="width-1">
                        <span>Số điện thoại:</span>
                        <input type="text" class="text-input" name="phone" id="phone">
                    </div>
                    <input type="hidden" name="nameSearch" value="${nameSearch}">
                    <div style="text-align: center; margin: 20px;">
                        <input type="submit" value="CẬP NHẬT" class="btn-user">
                    </div>
                    <div>
                        <span class="close-popup-btn" onclick="document.getElementsByClassName('popup-edit')[0].style.display = 'none';">&times;</span>
                    </div>
                </form>
            </div>
            <!-- Popup delete -->
            <div class="popup-delete">
                <div>
                    <span class="close-popup-delete-btn" onclick="document.getElementsByClassName('popup-delete')[0].style.display = 'none';">&times;</span>
                </div>
                <form action="<c:url value="/UserDelete" />" method="POST">
                    <div style="text-align: center">
                        <span>Bạn chắc chắn muốn xóa user <b id="usernameDetele"></b></span>
                    </div>
                    <input type="hidden" name="idDelete" id="idDelete">
                    <input type="hidden" name="nameSearch" value="${nameSearch}">
                    <input type="button" class="btn-user mt-5" value="Không" onclick="document.getElementsByClassName('popup-delete')[0].style.display = 'none';">
                    <input type="submit" class="btn-user ml-3" value="Có">
                </form>
            </div>
            <!-- Popup insert -->
            <div class="popup-insert">
                <form action="<c:url value="/UserInsert" />" method="POST" name="formInsert" onsubmit="return validateFormInsert()">
                    <h4 style="text-align: center;">THÊM THÔNG TIN USER</h4>
                    <div class="width-1">   
                        <span>Username:</span>
                        <input type="text" class="text-input" name="userName">
                    </div>
                    <div class="width-1">   
                        <span>Password:</span>
                        <input type="password" class="text-input" name="password">
                    </div>
                    <div class="width-1">
                        <span>Vai trò:</span>
                        <input type="text" class="text-input" name="role">
                    </div>
                    <div class="width-1">
                        <span>Tên người dùng:</span>
                        <input type="text" class="text-input" name="fullName">
                    </div>
                    <div class="width-1">
                        <span>Ngày sinh:</span>
                        <input type="text" class="text-input" name="dateOfBirth">
                    </div>
                    <div class="width-1">
                        <span>Email:</span>
                        <input type="text" class="text-input" name="email">
                    </div>
                    <div class="width-1">
                        <span>Số điện thoại:</span>
                        <input type="text" class="text-input" name="phone">
                    </div>
                    <input type="hidden" name="nameSearch" value="${nameSearch}">
                    <div style="text-align: center; margin: 20px;">
                        <input type="submit" value="THÊM MỚI" class="btn-user">
                    </div>
                    <div>
                        <span class="close-popup-btn" onclick="document.getElementsByClassName('popup-insert')[0].style.display = 'none';">&times;</span>
                    </div>
                </form>
            </div>

        </div>
    </body>
    <script>
        function closeSidenav() {
            var distance = '60px';
            document.getElementsByClassName("sidenav")[0].style.width = distance;
            document.getElementsByClassName("closeBtn")[0].style.display = 'none';
            document.getElementsByClassName("returnBtn")[0].style.display = 'block';
            document.getElementsByClassName("content")[0].style.left = distance;
            document.getElementsByClassName("logo-name")[0].style.display = 'none';
        }

        function openSidenav() {
            var distance = '200px';
            var op = document.getElementsByClassName("sidenav")[0];
            op.style.width = distance;
            document.getElementsByClassName("closeBtn")[0].style.display = 'block';
            document.getElementsByClassName("returnBtn")[0].style.display = 'none';
            document.getElementsByClassName("content")[0].style.left = distance;
            document.getElementsByClassName("logo-name")[0].style.display = 'inline';
        }

        function colourize() {
            var dnl = document.getElementsByTagName("tr");
            for (i = 0; i < dnl.length; i++) {
                if ((Math.round(i / 2) * 2) === ((i / 2) * 2))
                    dnl.item(i).style.background = "#F0F0F0";
            }
        }

        window.onload = colourize;

        function showErrMsg(msg) {
            console.log("msg:", mgs);
            if (msg !== "")
                alert(msg);
        }

        function showPopupEdit(user) {
            document.getElementById("_id").value = user.id;
            document.getElementById("fullName").value = user.fullName;
            document.getElementById("dateOfBirth").value = user.dateOfBirth;
            document.getElementById("email").value = user.email;
            document.getElementById("phone").value = user.phone;
            document.getElementById("userName").value = user.userName;
            document.getElementById("role").value = user.role;
            document.getElementsByClassName("popup-edit")[0].style.display = 'block';
        }

        function showPopupDelete(id, username) {
            document.getElementById("idDelete").value = id;
            document.getElementById("usernameDetele").innerHTML = username;
            document.getElementsByClassName("popup-delete")[0].style.display = 'block';
        }

        function showPopupInsert() {
            document.getElementsByClassName("popup-insert")[0].style.display = 'block';
        }

        function validateFormEdit() {
            var fullName = document.forms['formEdit']['fullName'].value;
            var date = document.forms['formEdit']['dateOfBirth'].value;
            var email = document.forms['formEdit']['email'].value;
            var phone = document.forms['formEdit']['phone'].value;
            console.log("4 thông tin:", document.forms['formEdit'], fullName, date, email, phone);
            var err = '';

            if (fullName === '') {
                err = 'Nhập thiếu tên người dùng';
                document.getElementById('fullName').focus()
            }

            if (date === '') {
                err = 'Nhập thiếu ngày tháng năm sinh';
                document.getElementById('dateOfBirth').focus()
            }

            if (email === '') {
                err = 'Nhập thiếu email';
                document.getElementById('email').focus()
            }

            if (phone === '') {
                err = 'Nhập thiếu số điện thoại';
                document.getElementById('phone').focus()
            }
            if (err === '')
                return true;
            else
            {
                alert(err);
                return false;
            }
        }

        function validateFormInsert() {
            var fullName = document.forms['formInsert']['fullName'].value;
            var date = document.forms['formInsert']['dateOfBirth'].value;
            var email = document.forms['formInsert']['email'].value;
            var phone = document.forms['formInsert']['phone'].value;
            console.log("4 thông tin:", document.forms['formInsert'], fullName, date, email, phone);
            var err = '';

            if (fullName === '') {
                err = 'Nhập thiếu tên người dùng';
                document.getElementById('fullName').focus()
            }

            if (date === '') {
                err = 'Nhập thiếu ngày tháng năm sinh';
                document.getElementById('dateOfBirth').focus()
            }

            if (email === '') {
                err = 'Nhập thiếu email';
                document.getElementById('email').focus()
            }

            if (phone === '') {
                err = 'Nhập thiếu số điện thoại';
                document.getElementById('phone').focus()
            }
            if (err === '')
                return true;
            else
            {
                alert(err);
                return false;
            }
        }

        setInterval(function () {
            var value = document.getElementById("msg-update").value;
            if (value != "") {
                console.log("vào đây");
                setTimeout(function () {
                    document.getElementById("msg-update").innerHTML = "";
                }, 2000);
            }
        }, 50);

        var openSubMenu = new Vue({
            el: '.open-sub-menu',
            data: {
                show: false,
                style: '',
            },
            methods: {
                showSubMenu: function () {
                    this.show = !this.show;
                    if (this.show === false) {
                        this.style = 'height: 0'
                    } else
                        this.style = 'height: auto'
                }
            }
        });

        var table = new Vue({
            el: '.table-scrollable'
        });

        var popupEdit = new Vue({
            el: '.popup-edit'
        });
    </script>
</html>