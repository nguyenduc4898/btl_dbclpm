<%-- 
    Document   : theo_doi_diem
    Created on : Feb 26, 2020, 2:27:45 PM
    Author     : NguyenDuc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Follow By Semester</title>
        <script src="js/vue.js"></script>
        <link rel="stylesheet" type="text/css" href="css/followScore.css">
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    </head>
    <body>
        <!--sidenav-->
        <div class="sidenav" id="mySidenav">
            <div class="item item-without-submenu">
                <form action="<c:url value="/User"/>" method="GET">
                    <input type="submit" value="Cập nhật user">
                </form>
            </div>
            <div class="item open-sub-menu">
                <div class="main-item" v-on:click="showSubMenu">Theo dõi điểm</div>
                <div class="sub-menu" v-bind:style="style">
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySubject"/>" method="GET">
                            <input type="submit" value="Theo dõi theo môn">
                        </form>
                    </div>
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySemester" />" method="GET">
                            <input type="submit" value="Theo dõi theo kì">
                        </form>
                    </div>
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySemesterChart" />">
                            <input type="submit" value="Thống kê điểm">
                        </form>
                    </div>
                </div>
            </div>
            <div class="item item-without-submenu">
                <form action="<c:url value="/SetupConfig"/>" method="GET">
                    <input type="submit" value="Cấu hình điểm">
                </form>
            </div>
            <a class="closeBtn" onclick="closeSidenav()">&times;</a>
            <img src="image/unnamed.png" class="img" alt="logo ptit" width="50" height="50">
            <a class="logo-name" href="image/">QUẢN LÝ ĐIỂM</a>
            <a class="returnBtn" href="#" onclick="openSidenav()">></a>
        </div>
        <!--end sidenav -->
        <!--topnav -->
        <div class="topnav">
            <div class="topnav-content">
                <div class="topnav-item">Trang chủ</div>
                <div class="topnav-item">Thông tin</div>
                <div class="topnav-item">
                    NguyenDuc <img style="position: absolute; top: 12px; margin-left: 19px"src="image/user.png" alt="user avatar" width="30" height="30">
                </div>
            </div>
        </div>
        <!--end topnav -->
        <!--content-->
        <div class="content">
            <h3>Thống kê điểm</h3>
            <form action="<c:url value="/FollowBySemesterChart" />" method="POST" class="form-search">
                <div style="display: inline-block">
                    <div class="drop-down-semester" style="margin-left: 0;" v-on:click="showDropDown = !showDropDown;">
                        {{selectedSemester}}
                        <div class="semester" v-if="showDropDown">
                            <a v-for="lm in listSemester" v-on:click="selectedSemester=lm" name="semester"> {{lm}} </a>
                        </div>
                        <input type="hidden" name="semester" v-model="selectedSemester">
                    </div>
                </div>

                <div style="display: inline-block;">
                    <div class="drop-down-year" v-on:click="showDropDown = !showDropDown;">
                        {{selectedYear}}
                        <div class="year" v-if="showDropDown">
                            <c:forEach var="year" items="${listYear}">
                                <a v-on:click="selectedYear=${year.startYear}+' - '+${year.endYear}">
                                    ${year.startYear} - ${year.endYear}
                                </a>
                            </c:forEach>
                        </div>
                        <input type="hidden" name="year" v-model="selectedYear">
                    </div>
                </div>
                
                <input class="search-btn" style="display: inline-block; margin-left: 20px;" type="submit" value="Tìm kiếm">
                <p class="err-msg">${msgErr}</p>
            </form>
            <div style="position: relative; z-index: -1;">
                <input type="hidden" value="${chartSubjectName}" id="chartSubjectName">
                <input type="hidden" value="${chartAvgScore}" id="chartAvgScore">
                <div id="bars" style="width: 800px; height: 400px;"></div>
                <div id="lines" style="width: 800px; height: 500px;"></div>
            </div>
        </div>

    </body>
    <script type="text/javascript">
        function closeSidenav() {
            var distance = '60px';
            document.getElementsByClassName("sidenav")[0].style.width = distance;
            document.getElementsByClassName("closeBtn")[0].style.display = 'none';
            document.getElementsByClassName("returnBtn")[0].style.display = 'block';
            document.getElementsByClassName("topnav")[0].style.left = distance;
            document.getElementsByClassName("content")[0].style.left = distance;
            document.getElementsByClassName("logo-name")[0].style.display = 'none';
        }

        function openSidenav() {
            var distance = '200px';
            var op = document.getElementsByClassName("sidenav")[0];
            op.style.width = distance;
            document.getElementsByClassName("closeBtn")[0].style.display = 'block';
            document.getElementsByClassName("returnBtn")[0].style.display = 'none';
            document.getElementsByClassName("topnav")[0].style.left = distance;
            document.getElementsByClassName("content")[0].style.left = distance;
            document.getElementsByClassName("logo-name")[0].style.display = 'inline';
        }

        function colourize() {
            var dnl = document.getElementsByTagName("tr");
            for (i = 0; i < dnl.length; i++) {
                if ((Math.round(i / 2) * 2) === ((i / 2) * 2))
                    dnl.item(i).style.background = "#F0F0F0";
            }
        }
        ;
        window.onload = colourize;

        function showErrMsg(msg) {
            console.log("msg:", mgs);
            if (msg !== "")
                alert(msg);
        }

        var openSubMenu = new Vue({
            el: '.open-sub-menu',
            data: {
                show: false,
                style: '',
            },
            methods: {
                showSubMenu: function () {
                    this.show = !this.show;
                    if (this.show === false) {
                        this.style = 'height: 0'
                    } else
                        this.style = 'height: 120px'
                }
            }
        });

        var dropDownMonth = new Vue({
            el: '.drop-down-month',
            data: {
                listMonth: [
                    "tháng 1",
                    "tháng 2",
                    "tháng 3",
                    "tháng 4",
                    "tháng 5",
                    "tháng 6",
                    "tháng 7",
                    "tháng 8",
                    "tháng 9",
                    "tháng 10",
                    "tháng 11",
                    "tháng 12",
                ],
                selectedMonth: 'Chọn tháng',
                showDropDown: false
            }
        });

        var dropDownYear = new Vue({
            el: '.drop-down-year',
            data: {
                selectedYear: "Chọn năm học",
                showDropDown: false
            }
        });

        var dropDownSemester = new Vue({
            el: '.drop-down-semester',
            data: {
                listSemester: ["Kì 1", "Kì 2", "Kì hè"],
                selectedSemester: "Chọn kì học",
                showDropDown: false
            }
        });

        var dropDown = new Vue({
            el: '.v-drop-down',
            data: {
                listItem: ["Xem tất cả các kì", "Xem kì đã chọn"],
                selectedItem: "Chọn cách xem",
                showDropDown: false
            }
        });

        // draw chart
        google.load("visualization", "1", {
            packages: ["corechart"]
        });

        // google.setOnLoadCallback(drawChart);

        function drawChart() {
            // initializing the array with the data
            var chartSubjectName = document.getElementById("chartSubjectName").value;
            console.log("chartSubjectName vừa nhận:", chartSubjectName);
            var chartAvgScore = document.getElementById("chartAvgScore").value;
            console.log("chartAvgScore vừa nhận:", chartAvgScore);

            chartSubjectName = String(chartSubjectName).replace("[", "");
            chartSubjectName = String(chartSubjectName).replace("]", "");
            chartSubjectName = chartSubjectName.split(",");

            chartAvgScore = String(chartAvgScore).replace("[", "");
            chartAvgScore = String(chartAvgScore).replace("]", "");
            chartAvgScore = chartAvgScore.split(",");

            console.log(chartSubjectName);
            console.log(chartAvgScore);

            var labelNameY = "Tên môn học";
            var labelNameX = "Điểm trung bình"
            var dataPrepare = [
                [labelNameY, labelNameX, {role: 'style'}]
            ];
            if (chartSubjectName.length > 1) {
                for (var i = 0; i < chartSubjectName.length; i++) {
                    dataPrepare.push([
                        chartSubjectName[i],
                        parseFloat(chartAvgScore[i]),
                        "7fdff6"
                    ]);
                }
                console.log(dataPrepare);
                var data = google.visualization.arrayToDataTable(dataPrepare);
                console.log(data);

                // options for the Bar chart
                var options1 = {
                    title: 'Biểu đồ cột',
                    bar: {groupWidth: '30%'},
                    vAxis: {
                        title: labelNameY,
                        titleTextStyle: {
                            color: 'red'
                        }
                    } // vertical subtitle
                };

                // instantiating and drawing the Bar chart
                var barras = new google.visualization.ColumnChart(document.getElementById('bars'));
                barras.draw(data, options1);
                // options for the Line chart
                var options2 = {
                    title: 'Biểu đồ đường',
                    hAxis: {
                        title: labelNameY,
                        titleTextStyle: {
                            color: 'red'
                        }
                    } // vertical subtitle
                };

                // instantiating and drawing the Line chart
                var linhas = new google.visualization.LineChart(document.getElementById('lines'));
                linhas.draw(data, options2);
            }
        }
        window.onload = drawChart;
    </script>
</html>