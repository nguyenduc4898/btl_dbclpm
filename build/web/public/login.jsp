<%-- 
    Document   : login
    Created on : Feb 26, 2020, 10:55:33 AM
    Author     : NguyenDuc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="css/login.css">
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
</head>
<body>
<div class="container">
    <div class="title">
        LOGIN
    </div>
    <form class="form" action="<c:url value='/LoginServlet' />" method="POST">
        <div class="label">User name:</div>
        <input type="text" name="userName">
        <br>
        <div class="label">Password:</div>
        <input type="password" name="password">
        <input class="btn-submit" type="submit" value="LOG IN">
        <p class="err-msg">${loginMessage}</p>
    </form>
    <div class="sign-in">
        <a href="#">New User? Sign in</a>
        <a href="#">Forgot your password?</a>
    </div>
</div>
</body>
</html>
