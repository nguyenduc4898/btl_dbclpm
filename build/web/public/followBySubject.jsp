<%-- 
    Document   : theo_doi_diem
    Created on : Feb 26, 2020, 2:27:45 PM
    Author     : NguyenDuc
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Follow By Subject</title>
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
        <link rel="stylesheet" type="text/css" href="css/followScore.css">
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    </head>
    <body>
        <!--sidenav-->
        <div class="sidenav" id="mySidenav">
            <div class="item item-without-submenu">
                <form action="<c:url value="/User"/>" method="GET">
                    <input type="submit" value="Cập nhật user">
                </form>
            </div>
            <div class="item open-sub-menu">
                <div class="main-item" v-on:click="showSubMenu">Theo dõi điểm</div>
                <div class="sub-menu" v-bind:style="style">
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySubject"/>" method="GET">
                            <input type="submit" value="Theo dõi theo môn">
                        </form>
                    </div>
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySemester" />" method="GET">
                            <input type="submit" value="Theo dõi theo kì">
                        </form>
                    </div>
                    <div class="sub-item">
                        <form action="<c:url value="/FollowBySemesterChart" />">
                            <input type="submit" value="Thống kê điểm">
                        </form>
                    </div>
                </div>
            </div>
            <div class="item item-without-submenu">
                <form action="<c:url value="/SetupConfig"/>" method="GET">
                    <input type="submit" value="Cấu hình điểm">
                </form>
            </div>
            <a class="closeBtn" onclick="closeSidenav()">&times;</a>
            <img src="image/unnamed.png" class="img" alt="logo ptit" width="50" height="50">
            <a class="logo-name" href="image/">QUẢN LÝ ĐIỂM</a>
            <a class="returnBtn" href="#" onclick="openSidenav()">></a>
        </div>
        <!--end sidenav -->
        <!--topnav-->
        <div class="topnav">
            <div class="topnav-content">
                <div class="topnav-item">Trang chủ</div>
                <div class="topnav-item">Thông tin</div>
                <div class="topnav-item">
                    NguyenDuc <img style="position: absolute; top: 12px; margin-left: 19px"src="image/user.png" alt="user avatar" width="30" height="30">
                </div>
            </div>
        </div>
        <!--end topnav -->

        <!--content-->
        <div class="content" style="z-index: 0;">
            <h3>Theo dõi điểm theo môn học</h3>
            <form action="<c:url value="/FollowBySubject" />" method="POST" class="form-search">
                <div style="display: inline-block">
                    <div style="margin-left: 0" class="drop-down-semester" v-on:click="showDropDown = !showDropDown;">
                        {{selectedSemester}}
                        <div class="semester" v-if="showDropDown">
                            <a v-for="lm in listSemester" v-on:click="selectedSemester=lm" name="semester"> {{lm}} </a>
                        </div>
                        <input type="hidden" name="semester" v-model="selectedSemester">
                    </div>
                </div>

                <div style="display: inline-block;">
                    <div class="drop-down-year" v-on:click="showDropDown = !showDropDown;">
                        {{selectedYear}}
                        <div class="year" v-if="showDropDown">
                            <c:forEach var="year" items="${listYear}">
                                <a v-on:click="selectedYear=${year.startYear}+' - '+${year.endYear}">
                                    ${year.startYear} - ${year.endYear}
                                </a>
                            </c:forEach>
                        </div>
                        <input type="hidden" name="year" v-model="selectedYear">
                    </div>
                </div>

                <div style="display: inline-block">
                    <input style="margin-left: 15px" type="text" class="text-input" name="subjectName" placeholder=" Nhập tên môn học" value="${subjectName}">
                    <a style="margin-left: 15px; color: #0099ff; cursor: pointer" onclick="showPopup(1)">Tra cứu tên môn học</a>
                </div>

                <input class="search-btn" type="submit" value="Tìm kiếm">
                <p class="err-msg">${msgErr}</p>
            </form>

            <!-- Iframe -->
            <div class="list-subject-iframe">
                <div>
                    <a class="closeBtn" style="color: black; top: 0; right: 30px; font-size: 30px" onclick="showPopup(0)">&times;</a>
                </div>
                <div>
                    <iframe frameBorder="0" src="public/list_subject.html" height="500px" width="500px"></iframe>
                </div>
            </div>
            <table border="0" cellpadding="0" cellspacing="0" v-if='${showTable}'>
                <tr>
                    <th width="5%">STT</th>
                    <th width="13%">Mã SV</th>
                    <th width="15%">Tên sinh viên</th>
                    <th width="10%">Tên môn học</th>
                    <th width="7%">Số tín chỉ</th>
                    <th width="8%">Điểm tp 1</th>
                    <th width="8%">Điểm tp 2</th>
                    <th width="8%">Điểm tp 3</th>
                    <th width="8%">Điểm cuối kì</th>
                    <th width="8%">Điểm trung bình</th>
                    <th width="8%">Điểm bằng chữ</th>
                </tr>
                <c:forEach var="score" items="${subjectScore}">
                    <tr>
                        <td>${score.stt}</td>
                        <td>${score.studentCode}</td>
                        <td>${score.studentName}</td>
                        <td>${score.subjectName}</td>
                        <td>${score.numCredit}</td>
                        <td>${score.score1}</td>
                        <td>${score.score2}</td>
                        <td>${score.score3}</td>
                        <td>${score.finalScore}</td>
                        <td>${score.tb}</td>
                        <td>${score.letter}</td>
                    </tr>
                </c:forEach>
            </table>

            <div class="show-dowload">
                <!--<div class="btn" style="text-align: center; padding-top: 10px; box-sizing: border-box;">Xuất ra excel</div>-->
                <form action="<c:url value="/ExportScore" />" method="POST">
                    <input type="hidden" name="subjectScore" value="${subjectScore}">
                    <input class="btn" style="text-align: center; box-sizing: border-box;" type="submit" value="Xuất ra excel" v-if="${showExport}">
                    <a class="btn" style="text-align: center; text-decoration: none; box-sizing: border-box; padding-top: 7px;" href="public/bangdiemtp.xls" download v-if="${showDownloadBtn}">Download</a>
                </form>
            </div>
        </div>
    </body>
    <script>
        window.addEventListener("message", function (event) {
            console.log("Nhận d-mail: ", event.data);
            if (event.data.subjectName)
                document.getElementsByClassName("text-input")[0].value = event.data.subjectName;
        });

        function closeSidenav() {
            var distance = '60px';
            document.getElementsByClassName("sidenav")[0].style.width = distance;
            document.getElementsByClassName("closeBtn")[0].style.display = 'none';
            document.getElementsByClassName("returnBtn")[0].style.display = 'block';
//            document.getElementsByClassName("topnav")[0].style.left = distance;
            document.getElementsByClassName("content")[0].style.left = distance;
            document.getElementsByClassName("logo-name")[0].style.display = 'none';
        }

        function openSidenav() {
            var distance = '200px';
            var op = document.getElementsByClassName("sidenav")[0];
            op.style.width = distance;
            document.getElementsByClassName("closeBtn")[0].style.display = 'block';
            document.getElementsByClassName("returnBtn")[0].style.display = 'none';
//            document.getElementsByClassName("topnav")[0].style.left = distance;
            document.getElementsByClassName("content")[0].style.left = distance;
            document.getElementsByClassName("logo-name")[0].style.display = 'inline';
        }

        function colourize() {
            var dnl = document.getElementsByTagName("tr");
            for (i = 0; i < dnl.length; i++) {
                if ((Math.round(i / 2) * 2) === ((i / 2) * 2))
                    dnl.item(i).style.background = "#F0F0F0";
            }
        }

        window.onload = colourize;

        function showErrMsg(msg) {
            console.log("msg:", mgs);
            if (msg !== "")
                alert(msg);
        }

        function showPopup(x) {
            if (x === 1)
                document.getElementsByClassName("list-subject-iframe")[0].style.display = 'block';
            if (x === 0)
                document.getElementsByClassName("list-subject-iframe")[0].style.display = 'none';
        }

        var openSubMenu = new Vue({
            el: '.open-sub-menu',
            data: {
                show: false,
                style: '',
            },
            methods: {
                showSubMenu: function () {
                    this.show = !this.show;
                    if (this.show === false) {
                        this.style = 'height: 0'
                    } else
                        this.style = 'height: auto'
                }
            }
        });

        var dropDownYear = new Vue({
            el: '.drop-down-year',
            data: {
                selectedYear: "Chọn năm học",
                showDropDown: false
            }
        });

        var dropDownSemester = new Vue({
            el: '.drop-down-semester',
            data: {
                listSemester: ["Kì 1", "Kì 2", "Kì hè"],
                selectedSemester: "Chọn kì học",
                showDropDown: false
            }
        });

        var showDownload = new Vue({
            el: '.show-dowload',
            data: {
                showDownload: false
            }
        });

        var table = new Vue({
            el: 'table'
        });
    </script>
</html>