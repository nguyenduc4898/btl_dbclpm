/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NguyenDuc
 */
public class Year {
    private int idYear;
    private int startYear;
    private int endYear;

    public Year() {
    }

    public Year(int idYear, int startYear, int endYear) {
        this.idYear = idYear;
        this.startYear = startYear;
        this.endYear = endYear;
    }

    public int getIdYear() {
        return idYear;
    }

    public void setIdYear(int idYear) {
        this.idYear = idYear;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }
    
    
}
