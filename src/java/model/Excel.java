/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import jxl.Workbook;
import jxl.biff.FontRecord;
import jxl.format.Colour;
import jxl.format.Font;
import jxl.write.Label;
import jxl.write.WritableCell;
import jxl.write.WritableCellFormat;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

/**
 *
 * @author NguyenDuc
 */
public class Excel {

    public void exportSubjectScoreToExcel(String path, ArrayList<Subject> subjectScore) throws IOException, WriteException {
        try {
            File f = new File(path);
            System.out.println(f.getAbsolutePath());
            WritableWorkbook workbook = Workbook.createWorkbook(f);
            WritableSheet sheet = workbook.createSheet("sheetDucnm", 0);

            sheet.addCell(new Label(0, 0, "Bảng điểm thành phân môn" + subjectScore.get(0).getSubjectName()));
            sheet.addCell(new Label(0, 1, "STT"));
            sheet.addCell(new Label(1, 1, "Mã SV"));
            sheet.addCell(new Label(2, 1, "Tên sinh viên"));
            sheet.addCell(new Label(3, 1, "Tên môn học"));
            sheet.addCell(new Label(4, 1, "Số tín chỉ"));
            sheet.addCell(new Label(5, 1, "Điểm tp 1"));
            sheet.addCell(new Label(6, 1, "Điểm tp 2"));
            sheet.addCell(new Label(7, 1, "Điểm tp 3"));
            sheet.addCell(new Label(8, 1, "Điểm cuối kì"));
            sheet.addCell(new Label(9, 1, "Điểm trung bình"));
            sheet.addCell(new Label(10, 1, "Điểm bằng chữ"));

            WritableCell c = sheet.getWritableCell(0, 0);
            WritableCellFormat newFormat = new WritableCellFormat();
            newFormat.setBackground(Colour.GRAY_25);
            c.setCellFormat(newFormat);

            for (int i = 0; i < subjectScore.size(); i++) {
                Subject s = subjectScore.get(i);
                int stt = s.getStt();
                String studentCode = s.getStudentCode();
                String studentName = s.getStudentName();
                String subjectName = s.getSubjectName();
                int numCredit = s.getNumCredit();
                int score1 = s.getScore1();
                int score2 = s.getScore2();
                int score3 = s.getScore3();
                int finalScore = s.getFinalScore();
                float tb = s.getTb();
                String letter = s.getLetter();

                sheet.addCell(new Label(0, i + 2, String.valueOf(stt)));
                sheet.addCell(new Label(1, i + 2, String.valueOf(studentCode)));
                sheet.addCell(new Label(2, i + 2, String.valueOf(studentName)));
                sheet.addCell(new Label(3, i + 2, String.valueOf(subjectName)));
                sheet.addCell(new Label(4, i + 2, String.valueOf(numCredit)));
                sheet.addCell(new Label(5, i + 2, String.valueOf(score1)));
                sheet.addCell(new Label(6, i + 2, String.valueOf(score2)));
                sheet.addCell(new Label(7, i + 2, String.valueOf(score3)));
                sheet.addCell(new Label(8, i + 2, String.valueOf(finalScore)));
                sheet.addCell(new Label(9, i + 2, String.valueOf(tb)));
                sheet.addCell(new Label(10, i + 2, String.valueOf(letter)));
            }

            workbook.write();
            workbook.close();
            System.out.println("Ket thuc ghi file");
        } catch (Exception e) {
            System.out.println("exportSubjectScoreToExcel err:" + e.toString());
        }
    }
}
