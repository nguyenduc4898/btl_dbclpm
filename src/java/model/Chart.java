/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NguyenDuc
 */
public class Chart {
    private String subjectName;
    private float avgScore;

    public Chart(String subjectName, float avgScore) {
        this.subjectName = subjectName;
        this.avgScore = avgScore;
    }

    public Chart() {
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public float getAvgScore() {
        return avgScore;
    }

    public void setAvgScore(float avgScore) {
        this.avgScore = avgScore;
    }
}
