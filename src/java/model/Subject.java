/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NguyenDuc
 */
public class Subject {
    private int stt;
    private String studentCode;
    private String studentName;
    private String subjectName;
    private int score1;
    private int score2;
    private int score3;
    private int finalScore;
    private float tb;
    private String letter;
    private int numCredit;
    private String semesterName;
    private int startYear;
    private int endYear;

    public Subject(int stt, String studentCode, String studentName, String subjectName, int score1, int score2, int score3, int finalScore, float tb, String letter, int numCredit, String semesterName, int startYear, int endYear) {
        this.stt = stt;
        this.studentCode = studentCode;
        this.studentName = studentName;
        this.subjectName = subjectName;
        this.score1 = score1;
        this.score2 = score2;
        this.score3 = score3;
        this.finalScore = finalScore;
        this.tb = tb;
        this.letter = letter;
        this.numCredit = numCredit;
        this.semesterName = semesterName;
        this.startYear = startYear;
        this.endYear = endYear;
    }

    public String getLetter() {
        return letter;
    }

    public void setLetter(String letter) {
        this.letter = letter;
    }

    public float getTb() {
        return tb;
    }

    public void setTb(float tb) {
        this.tb = tb;
    }
    
    public Subject() {
    }
    
    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
    
    public int getStt() {
        return stt;
    }

    public void setStt(int stt) {
        this.stt = stt;
    }

    public String getStudentCode() {
        return studentCode;
    }

    public void setStudentCode(String studentCode) {
        this.studentCode = studentCode;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public int getScore1() {
        return score1;
    }

    public void setScore1(int score1) {
        this.score1 = score1;
    }

    public int getScore2() {
        return score2;
    }

    public void setScore2(int score2) {
        this.score2 = score2;
    }

    public int getScore3() {
        return score3;
    }

    public void setScore3(int score3) {
        this.score3 = score3;
    }

    public int getFinalScore() {
        return finalScore;
    }

    public void setFinalScore(int finalScore) {
        this.finalScore = finalScore;
    }

    public int getNumCredit() {
        return numCredit;
    }

    public void setNumCredit(int numCredit) {
        this.numCredit = numCredit;
    }

    public String getSemesterName() {
        return semesterName;
    }

    public void setSemesterName(String semesterName) {
        this.semesterName = semesterName;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    @Override
    public String toString() {
        return "Subject{" + "stt=" + stt + ", studentCode=" + studentCode + ", studentName=" + studentName + ", subjectName=" + subjectName + ", score1=" + score1 + ", score2=" + score2 + ", score3=" + score3 + ", finalScore=" + finalScore + ", tb=" + tb + ", letter=" + letter + ", numCredit=" + numCredit + ", semesterName=" + semesterName + ", startYear=" + startYear + ", endYear=" + endYear + '}';
    }
}