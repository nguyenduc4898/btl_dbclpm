/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author NguyenDuc
 */
public class Semester {
    private int idSemester;
    private String semesterName;
    private int idYear;
    private int startYear;
    private int endYear;

    public Semester(int idSemester, String semesterName, int idYear, int startYear, int endYear) {
        this.idSemester = idSemester;
        this.semesterName = semesterName;
        this.idYear = idYear;
        this.startYear = startYear;
        this.endYear = endYear;
    }

    public int getStartYear() {
        return startYear;
    }

    public void setStartYear(int startYear) {
        this.startYear = startYear;
    }

    public int getEndYear() {
        return endYear;
    }

    public void setEndYear(int endYear) {
        this.endYear = endYear;
    }

    public Semester() {
    }

    public int getIdSemester() {
        return idSemester;
    }

    public void setIdSemester(int idSemester) {
        this.idSemester = idSemester;
    }

    public String getSemesterName() {
        return semesterName;
    }

    public void setSemesterName(String semesterName) {
        this.semesterName = semesterName;
    }

    public int getIdYear() {
        return idYear;
    }

    public void setIdYear(int idYear) {
        this.idYear = idYear;
    }

    @Override
    public String toString() {
        return "Semester{" + "idSemester=" + idSemester + ", semesterName=" + semesterName + ", idYear=" + idYear + ", startYear=" + startYear + ", endYear=" + endYear + '}';
    }

}
