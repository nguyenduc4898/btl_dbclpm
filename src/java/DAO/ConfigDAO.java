/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.SimpleDateFormat;
import java.util.Date;
import model.User;

/**
 *
 * @author NguyenDuc
 */
public class ConfigDAO {
    public boolean updateConfigSubject(String tp1, String tp2, String tp3, String fi, String subjectName) throws Exception {
        try {
            DAO d = new DAO();
            Connection con = d.getConnection();
            String sql = "update quan_ly_diem_ptit.monhoc "
                    + "set tp1 = ?, tp2 = ?, tp3 = ?, cuoiKi = ? "
                    + "where ten = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            
            ps.setInt(1, Integer.parseInt(tp1));
            ps.setInt(2, Integer.parseInt(tp2));
            ps.setInt(3, Integer.parseInt(tp3));
            ps.setInt(4, Integer.parseInt(fi));
            ps.setString(5, subjectName);

            int row = ps.executeUpdate();
            if (row != 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("updateInfoUser err: " + e.toString());
            return false;
        }
    }
}
