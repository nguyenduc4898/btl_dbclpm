/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import com.sun.corba.se.impl.activation.ServerMain;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author NguyenDuc
 */
public class DAO {

    private Connection conn;

    public Connection getConnection() throws Exception {
        String dbURL = "jdbc:mysql://localhost:3306/quan_ly_diem_ptit?useSSL=false";
        String userName = "root";
        String password = "1234";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            System.out.println(dbURL);
            conn = DriverManager.getConnection(dbURL, userName, password);
            System.out.println("connect thanh cong!");
        } catch (Exception ex) {
            System.out.println("connect that bai");
            ex.printStackTrace();
        }
        return conn;
    }
}
