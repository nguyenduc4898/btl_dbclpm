/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.User;
import model.Year;

/**
 *
 * @author NguyenDuc
 */
public class UserDAO {

    public ArrayList getInfoUser(String fullName) throws Exception {
        try {
            ArrayList<User> users = new ArrayList<User>();
            DAO d = new DAO();
            Connection con = d.getConnection();
            fullName = "%" + fullName + "%";
            String sql = "select * from nguoidung where hoTen like ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, fullName);
            ResultSet rs = ps.executeQuery();
            int i = 1;
            while (rs.next()) {
                User user = new User();
                user.setStt(i);
                user.setId(rs.getInt("id"));
                user.setFullName(rs.getString("hoTen"));
                user.setDateOfBirth(rs.getString("ngaySinh"));
                user.setEmail(rs.getString("email"));
                user.setPhone(rs.getString("sdt"));
                user.setUserName(rs.getString("username"));
                user.setPass(rs.getString("password"));
                user.setRole(rs.getInt("vaiTro") == 1 ? "sinh viên" : "nhân viên");
                users.add(user);
                i++;
            }
            return users;
        } catch (Exception e) {
            System.out.println("getInfoUser err: " + e.toString());
            return null;
        }
    }

    public boolean getInfoUserLogin(String username, String password) throws Exception {
        try {
            DAO d = new DAO();
            Connection con = d.getConnection();
            String sql = "select * from nguoidung where username = ? and password = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return true;
            }
            return false;
        } catch (Exception e) {
            System.out.println("getInfoUser err: " + e.toString());
            return false;
        }
    }

    public boolean updateInfoUser(User user) throws Exception {
        try {
            DAO d = new DAO();
            Connection con = d.getConnection();
            String sql = "update quan_ly_diem_ptit.nguoidung\n"
                    + "set hoTen = ?, ngaySinh = ?, email = ?, sdt=?"
                    + "where id = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, user.getFullName());

            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(user.getDateOfBirth());
            ps.setDate(2, (new java.sql.Date(date1.getTime())));
            ps.setString(3, user.getEmail());
            ps.setString(4, user.getPhone());
            ps.setInt(5, user.getId());

            int row = ps.executeUpdate();
            if (row != 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("updateInfoUser err: " + e.toString());
            return false;
        }
    }

    public boolean deleteUserInfo(int id) throws Exception {
        try {
            DAO d = new DAO();
            Connection con = d.getConnection();
            String sql = "delete from quan_ly_diem_ptit.nguoidung"
                    + " where id = ?";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id);

            int row = ps.executeUpdate();
            if (row != 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("deleteUserInfo err: " + e.toString());
            return false;
        }
    }

    public boolean insertUserInfo(String userName, String password, String role, String fullName, String dateOfBirth, String email, String phone) {
        try {
            DAO d = new DAO();
            Connection con = d.getConnection();
            String sql = "insert into quan_ly_diem_ptit.nguoidung(hoTen, ngaySinh, email, sdt, username, password, vaiTro) "
                    + "values (?, ?, ?, ?, ?, ?, ?);";
            PreparedStatement ps = con.prepareStatement(sql);

            System.out.println("sql insert:" + sql);

            ps.setString(1, fullName);
            Date date1 = new SimpleDateFormat("yyyy-MM-dd").parse(dateOfBirth);
            ps.setDate(2, (new java.sql.Date(date1.getTime())));
            ps.setString(3, email);
            ps.setString(4, phone);
            ps.setString(5, userName);
            ps.setString(6, password);
            ps.setInt(7, role.equals("sinh viên") ? 1 : 2);
            System.out.println("PS:" + ps.toString());

            int row = ps.executeUpdate();
            System.out.println("ROW KHI INSERT:" + String.valueOf(row));
            if (row != 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            System.out.println("insertUserInfo err: " + e.toString());
            return false;
        }
    }
}
