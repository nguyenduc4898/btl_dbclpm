/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import static DAO.SubjectDAO.convertScoreToLetter;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import model.Chart;
import model.Subject;

/**
 *
 * @author NguyenDuc
 */
public class ChartDAO {

    public ArrayList getAvgScoreBySemester(String semester, int startYear, int endYear) {
        try {
            System.out.println("start getAvgScore");
            ArrayList<Chart> charts = new ArrayList<Chart>();
            DAO d = new DAO();
            Connection con = d.getConnection();

            SubjectDAO sd = new SubjectDAO();
            ArrayList<String> listSubject = sd.getListSubject(semester, startYear, endYear);
            for (String subject : listSubject) {
                String sql = "select round((dkh.tp1 * mh.tp1/100 + dkh.tp2 * mh.tp2/100 + dkh.tp3 * mh.tp3/100 + dkh.cuoiKi * mh.cuoiKi/100), 1) as avg, mh.ten\n"
                        + "from dangkihoc as dkh, lophocphan as lhp, chuongtrinhhoc as cth, monhoc as mh, hocky as hk, namhoc as nh\n"
                        + "where dkh.id_LHP = lhp.id and lhp.id_chuongtrinhhoc = cth.id and cth.idMonHoc = mh.id \n"
                        + "and hk.id = cth.id_HocKy and hk.id_NamHoc = nh.id and hk.ten = ? and nh.namBatDau = ? and nh.namKetThuc = ? and mh.ten = ?;";
                PreparedStatement ps = con.prepareStatement(sql);
                ps.setString(1, semester);
                ps.setInt(2, startYear);
                ps.setInt(3, endYear);
                ps.setString(4, subject);
                // System.out.println(ps);
                ResultSet rs = ps.executeQuery();
                float avg = 0;
                int i = 1;
                while (rs.next()){
                    avg += rs.getFloat("avg");
                    i++;
                }
                avg = (float)avg / i;
                DecimalFormat df = new DecimalFormat("#.#"); // làm tròn tới 2 số thập phân
                df.setRoundingMode(RoundingMode.CEILING);
                avg = Float.parseFloat(df.format(avg));
                System.out.println("AVG:" + avg);
                if (avg != 0.0) charts.add(new Chart(subject, avg));
            }
            con.close();
            return charts;
        } catch (Exception e) {
            System.out.println("getScoreByStudentCode err: " + e.toString());
            return null;
        }
    }
}
