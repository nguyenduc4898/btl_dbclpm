/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import model.Year;

/**
 *
 * @author NguyenDuc
 */
public class YearDAO {
    public ArrayList getYear()throws Exception {
        try{
            ArrayList<Year> years = new ArrayList<Year>();
            DAO d = new DAO();
            Connection con = d.getConnection();
            String sql = "select * from namhoc";
            PreparedStatement ps = con.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                Year y = new Year();
                y.setIdYear(rs.getInt("id"));
                y.setStartYear(rs.getInt("namBatDau"));
                y.setEndYear(rs.getInt("namKetThuc"));
                years.add(y);
            }
            return years;
        }
        catch (Exception e){
            System.out.println("getYear err: " + e.toString());
            return null;
        }
    }
}
