/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import model.*;

/**
 *
 * @author NguyenDuc
 */
public class SubjectDAO {

    public ArrayList getScoreByStudentCode(String semester, int startYear, int endYear, String studentCode) throws Exception {
        try {
            System.out.println(semester + "--" + startYear + "--" + endYear + "--" + studentCode);
            ArrayList<Subject> subjects = new ArrayList<Subject>();
            DAO d = new DAO();
            Connection con = d.getConnection();
            String sql = "SELECT sv.maSV as MaSV, mh.ten, dkh.tp1, dkh.tp2, dkh.tp3, dkh.cuoiKi, mh.soTinChi, hk.ten as tenKiHoc, nh.namBatDau, nh.namKetThuc, mh.tp1 as hs1, mh.tp2 as hs2, mh.tp3 as hs3, mh.cuoiKi as hsck\n"
                    + "FROM chuongtrinhhoc as cth, monhoc as mh, hocky as hk, namhoc as nh, dangkihoc as dkh, lophocphan as lhp, sinhvien as sv, nguoidung as nd\n"
                    + "where cth.idMonhoc = mh.id and hk.id = cth.id_HocKy and hk.id_NamHoc = nh.id \n"
                    + "and hk.ten = ? and nh.namBatDau = ? and nh.namKetThuc = ?\n"
                    + "and dkh.id_LHP = lhp.id and dkh.id_SinhVien = sv.id_NguoiDung and nd.id = sv.id_NguoiDung and lhp.id_chuongtrinhhoc = cth.id and sv.maSV = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, semester);
            ps.setInt(2, startYear);
            ps.setInt(3, endYear);
            ps.setString(4, studentCode);
//            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            int i = 1;
            while (rs.next()) {
                System.out.println("record " + i);
                Subject s = new Subject();
                s.setStt(i);
                s.setStudentCode(rs.getString("MaSV"));
                s.setSubjectName(rs.getString("ten"));
                s.setScore1(rs.getInt("tp1"));
                s.setScore2(rs.getInt("tp2"));
                s.setScore3(rs.getInt("tp3"));
                s.setFinalScore(rs.getInt("cuoiKi"));
                s.setNumCredit(rs.getInt("soTinChi"));
                s.setSemesterName(rs.getString("tenKiHoc"));
                s.setStartYear(rs.getInt("namBatDau"));
                s.setEndYear(rs.getInt("namKetThuc"));
                
                DecimalFormat df = new DecimalFormat("#.#"); // làm tròn 1 chữ số thập phân
                df.setRoundingMode(RoundingMode.CEILING);
                
                float tp1 = (float)((float)(rs.getInt("tp1")*rs.getInt("hs1"))/100);
                float tp2 = (float)((float)(rs.getInt("tp2")*rs.getInt("hs2"))/100);
                float tp3 = (float)((float)(rs.getInt("tp3")*rs.getInt("hs3"))/100);
                float cuoiKi = (float)((float)(rs.getInt("cuoiKi")*rs.getInt("hsck"))/100);
                System.out.println(tp1 + "/" + tp2 + "/" + tp3 + "/" + cuoiKi);
                s.setTb(Float.parseFloat(df.format(tp1 + tp2 + tp3 + cuoiKi)));
                s.setLetter(convertScoreToLetter(s.getTb()));
                
                subjects.add(s);
                i++;
            }
            con.close();
            return subjects;
        } catch (Exception e) {
            System.out.println("getScoreByStudentCode err: " + e.toString());
            return null;
        }
    }

    public ArrayList getScoreBySubject(String semester, int startYear, int endYear, String subjectName) {
        try {
            System.out.println("SUBJECT NAME:" + subjectName);
            ArrayList<Subject> subjects = new ArrayList<Subject>();
            DAO d = new DAO();
            Connection con = d.getConnection();
            String sql = "SELECT sv.maSV as MaSV, nd.hoTen as tenSV, mh.ten, dkh.tp1, dkh.tp2, dkh.tp3, dkh.cuoiKi, mh.soTinChi, hk.ten as tenKiHoc, nh.namBatDau, nh.namKetThuc, mh.tp1 as hs1, mh.tp2 as hs2, mh.tp3 as hs3, mh.cuoiKi as hsck\n"
                    + "FROM chuongtrinhhoc as cth, monhoc as mh, hocky as hk, namhoc as nh, dangkihoc as dkh, lophocphan as lhp, sinhvien as sv, nguoidung as nd\n"
                    + "where cth.idMonhoc = mh.id and hk.id = cth.id_HocKy and hk.id_NamHoc = nh.id \n"
                    + "and hk.ten = ? and nh.namBatDau = ? and nh.namKetThuc = ?\n"
                    + "and dkh.id_LHP = lhp.id and dkh.id_SinhVien = sv.id_NguoiDung and nd.id = sv.id_NguoiDung and lhp.id_chuongtrinhhoc = cth.id and mh.ten = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, semester);
            ps.setInt(2, startYear);
            ps.setInt(3, endYear);
            ps.setString(4, subjectName);
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();
            int i = 1;
            while (rs.next()) {
                System.out.println("record " + i);
                Subject s = new Subject();
                s.setStt(i);
                s.setStudentCode(rs.getString("MaSV"));
                s.setStudentName(rs.getString("tenSV"));
                s.setSubjectName(rs.getString("ten"));
                s.setScore1(rs.getInt("tp1"));
                s.setScore2(rs.getInt("tp2"));
                s.setScore3(rs.getInt("tp3"));
                s.setFinalScore(rs.getInt("cuoiKi"));
                s.setNumCredit(rs.getInt("soTinChi"));
                s.setSemesterName(rs.getString("tenKiHoc"));
                s.setStartYear(rs.getInt("namBatDau"));
                s.setEndYear(rs.getInt("namKetThuc"));
                
                DecimalFormat df = new DecimalFormat("#.#"); // làm tròn tới 2 số thập phân
                df.setRoundingMode(RoundingMode.CEILING);
                
                float tp1 = (float)((float)(rs.getInt("tp1")*rs.getInt("hs1"))/100);
                float tp2 = (float)((float)(rs.getInt("tp2")*rs.getInt("hs2"))/100);
                float tp3 = (float)((float)(rs.getInt("tp3")*rs.getInt("hs3"))/100);
                float cuoiKi = (float)((float)(rs.getInt("cuoiKi")*rs.getInt("hsck"))/100);
                System.out.println(tp1 + "/" + tp2 + "/" + tp3 + "/" + cuoiKi);
                s.setTb(Float.parseFloat(df.format(tp1 + tp2 + tp3 + cuoiKi)));
                s.setLetter(convertScoreToLetter(s.getTb()));
                
                subjects.add(s);
                i++;
            }
            con.close();
            return subjects;
        } catch (Exception e) {
            System.out.println("getScoreByStudentCode err: " + e.toString());
            return null;
        }
    }

    public ArrayList getListSubject(String semester, int startYear, int endYear) throws Exception {
        try {
            ArrayList<String> listSubjectName = new ArrayList<>();
            DAO d = new DAO();
            Connection con = d.getConnection();
            String sql = "SELECT mh.ten FROM hocky as hk, monhoc as mh, chuongtrinhhoc as cth, namhoc as nh\n"
                    + "where cth.idMonhoc = mh.id and cth.id_HocKy = hk.id \n"
                    + "and hk.id_NamHoc = nh.id and hk.ten = ? and nh.namBatDau = ? and nh.namKetThuc = ?;";
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, semester);
            ps.setInt(2, startYear);
            ps.setInt(3, endYear);
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                listSubjectName.add(rs.getString("ten"));
            }
            con.close();
            return listSubjectName;
        } catch (Exception e) {
            System.out.println("getListSubject err:" + e.toString());
            return null;
        }
    }

    public ArrayList getListSemester() throws Exception {
        try {
            ArrayList<Semester> listSemester = new ArrayList<>();
            DAO d = new DAO();
            Connection con = d.getConnection();
            String sql = "select * from hocky as hk, namhoc as nh\n"
                    + "where hk.id_NamHoc = nh.id;";
            PreparedStatement ps = con.prepareStatement(sql);
            System.out.println(ps);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Semester s = new Semester();
                s.setIdSemester(rs.getInt("id"));
                s.setSemesterName(rs.getString("ten"));
                s.setIdYear(rs.getInt("id_NamHoc"));
                s.setStartYear(rs.getInt("namBatDau"));
                s.setEndYear(rs.getInt("namKetThuc"));
                listSemester.add(s);
            }
            con.close();
            return listSemester;
        } catch (Exception e) {
            System.out.println("getListSubject err:" + e.toString());
            return null;
        }
    }
    
    public static String convertScoreToLetter(float score){
        if (score < 4.0) return "F";
        else if (4.0 <= score && score <= 4.9) return "D";
        else if (5.0 <= score && score <= 5.4) return "D+";
        else if (5.5 <= score && score <= 6.4) return "C";
        else if (6.5 <= score && score <= 6.9) return "C+";
        else if (7.0 <= score && score <= 7.9) return "B";
        else if (8.0 <= score && score <= 8.4) return "B+";
        else if (8.5 <= score && score <= 8.9) return "A";
        else return "A+";
    }
}
