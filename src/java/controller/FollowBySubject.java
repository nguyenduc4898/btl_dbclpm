package controller;

import DAO.SubjectDAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Subject;

/**
 *
 * @author NguyenDuc
 */
public class FollowBySubject extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "/public/followBySubject.jsp";
        try {
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html; charset=UTF-8");
            HttpSession session = request.getSession();

            String semester = request.getParameter("semester");
            String year = request.getParameter("year");
            String subjectName = request.getParameter("subjectName");

            System.out.println(semester + "--" + year + "--" + subjectName);

            // off các thành phần bảng, nút nếu có lỗi xảy ra
            request.setAttribute("showTable", false);
            request.setAttribute("showExport", false);
            session.setAttribute("showDownloadBtn", false);

            if (subjectName.equals("") == true) {
                System.out.println(" FollowBySubject check ma subjectName");
                request.setAttribute("msgErr", "Chưa nhập tên môn học");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
            } else {
                request.setAttribute("msgErr", "");
                session.setAttribute("subjectName", subjectName.trim());
            }

            if (semester.equals("Chọn kì học") == true) {
                System.out.println("FollowBySubject check ki hoc");
                request.setAttribute("msgErr", "Chưa chọn kì học");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
            } else {
                request.setAttribute("msgErr", "");
            }

            int startYear = 0;
            int endYear = 0;
            if (year.equals("Chọn năm học") == false) {
                System.out.println("FollowBySubject check nam hoc");
                request.setAttribute("msgErr", "");
                String[] yearSplit = year.split(" - ");
                startYear = Integer.parseInt(yearSplit[0]);
                endYear = Integer.parseInt(yearSplit[1]);
            } else {
                request.setAttribute("msgErr", "Chưa chọn năm học");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
            }

            System.out.println("statYear:" + startYear);
            System.out.println("endYear:" + endYear);

            /*Truy vấn db trả về dữ liệu*/
            ArrayList<Subject> subjectScore = new ArrayList<>();
            SubjectDAO sd = new SubjectDAO();
            try {
                subjectScore = sd.getScoreBySubject(semester, startYear, endYear, subjectName);
            } catch (Exception ex) {
                Logger.getLogger(FollowBySubject.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (subjectScore != null) {
                System.out.println("vào đây");
                session.setAttribute("subjectScore", subjectScore);
                if (subjectScore.isEmpty()) {
                    request.setAttribute("msgErr", "Không có dữ liệu về điểm của môn học này");
                } else {
                    request.setAttribute("showTable", true);
                    request.setAttribute("showExport", true);
                }
            } else {
                session.setAttribute("subjectScore", subjectScore);
                System.out.println("ss null");
            }
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
            dispatcher.forward(request, response);
        } catch (Exception e) {
            System.out.println("FollowBySubject err: " + e);
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        request.setAttribute("showTable", false);
        request.setAttribute("showExport", false);
        session.setAttribute("showDownloadBtn", false);
        String url = "/public/followBySubject.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }
}
