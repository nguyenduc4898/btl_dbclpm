/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author NguyenDuc
 */
public class UserDelete extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String idDelete = request.getParameter("idDelete");
        String nameSearch = request.getParameter("nameSearch");

        UserDAO ud = new UserDAO();

        try {
            boolean checkUpdate = ud.deleteUserInfo(Integer.parseInt(idDelete));
            if (checkUpdate) {
                request.setAttribute("msgUpdate", "Xóa thông tin thành công");
                request.setAttribute("colorMsg", "green");
            } else {
                request.setAttribute("msgUpdate", "Xóa thông tin thất bại");
                request.setAttribute("colorMsg", "red");
            }

            ArrayList<model.User> users = ud.getInfoUser(nameSearch);
            request.setAttribute("users", users);
            request.setAttribute("showTable", true);
        } catch (Exception ex) {
            Logger.getLogger(model.User.class.getName()).log(Level.SEVERE, null, ex);
        }
        String url = "/public/user.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }
}
