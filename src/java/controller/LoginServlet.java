/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.UserDAO;
import DAO.YearDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Year;

/**
 *
 * @author NguyenDuc
 */
public class LoginServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            String userName = request.getParameter("userName");
            System.out.println("User name:" + userName);
            String url = "/public/followBySubject.jsp";

            String password = request.getParameter("password");
            System.out.println("Password:" + password);

            /*
            get thông tin của user từ db ra để đăng nhập
             */
            UserDAO ud = new UserDAO();
            boolean checkUser = ud.getInfoUserLogin(userName, password);
            // test
            if (checkUser) {
                System.out.println("Đăng nhập thành công");
                // set thông tin cần thiết lên session
                setInfoSession(request);

                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
            } else {
                String errMessage = "Sai thông tin đăng nhập";
                request.setAttribute("loginMessage", errMessage);
                System.out.println("Login that bai");
                url = "/public/login.jsp";
                RequestDispatcher dispatcher
                        = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
            }
        } catch (Exception e) {
            System.out.println("Login servelet err:" + e.toString());
        }
    }

    public static void setInfoSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        // set session để chọn year
        ArrayList<Year> listYear = new ArrayList<>();
        YearDAO yd = new YearDAO();
        try {
            System.out.println("Vào đây");
            listYear = yd.getYear();
            System.out.println(listYear.size());
            session.setAttribute("listYear", listYear);
            for (Year y : listYear) {
                System.out.println(y.getStartYear() + "-" + y.getEndYear());
            }
            session.setAttribute("showDownloadBtn", false); // ẩn nút download khi load trang
            request.setAttribute("showTable", false);
            request.setAttribute("showExport", false);
        } catch (Exception ex) {
            Logger.getLogger(FollowBySubject.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "/public/login.jsp";
        RequestDispatcher dispatcher
                = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }
}
