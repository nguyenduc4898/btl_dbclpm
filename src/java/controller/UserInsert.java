/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author NguyenDuc
 */
public class UserInsert extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserDAO ud = new UserDAO();
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        String role = request.getParameter("role");
        String fullName = request.getParameter("fullName");
        String dateOfBirth = request.getParameter("dateOfBirth");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        try {
            boolean checkInsert = ud.insertUserInfo(userName, password, role, fullName, dateOfBirth, email, phone);
            if (checkInsert) {
                request.setAttribute("msgUpdate", "Thêm thông tin thành công");
                request.setAttribute("colorMsg", "green");
            } else {
                request.setAttribute("msgUpdate", "Thêm thông tin thất bại");
                request.setAttribute("colorMsg", "red");
            }
            request.setAttribute("showTable", true);
        } catch (Exception ex) {
            Logger.getLogger(model.User.class.getName()).log(Level.SEVERE, null, ex);
        }
        String url = "/public/user.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

}
