/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.ChartDAO;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Chart;

/**
 *
 * @author NguyenDuc
 */
@WebServlet(name = "FollowBySemesterChart", urlPatterns = {"/FollowBySemesterChart"})
public class FollowBySemesterChart extends HttpServlet {
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "/public/followBySemesterChart.jsp";
        try {
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html; charset=UTF-8");
            HttpSession session = request.getSession();

            String semester = request.getParameter("semester");
            String year = request.getParameter("year");

            System.out.println(semester + "**" + year);
            
            if (semester.equals("Chọn kì học")) {
                System.out.println("Chua chon ki hoc");
                request.setAttribute("msgErr", "Chưa chọn kì học");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
            }

            int startYear = 0;
            int endYear = 0;
            if (year.equals("Chọn năm học")) {
                System.out.println("Chua chon nam hoc");
                request.setAttribute("msgErr", "Chưa chọn năm học");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
            } else if (year.equals("Chọn năm học") == false) {
                request.setAttribute("msgErr", "");
                String[] yearSplit = year.split(" - ");
                startYear = Integer.parseInt(yearSplit[0]);
                endYear = Integer.parseInt(yearSplit[1]);
            }

            System.out.println("statYear:" + startYear);
            System.out.println("endYear:" + endYear);

            /*Truy vấn db trả về dữ liệu*/
            ArrayList<Chart> chartsBySemester = new ArrayList<>();
            ChartDAO cd = new ChartDAO();
            chartsBySemester = cd.getAvgScoreBySemester(semester, startYear, endYear);
            if (chartsBySemester != null) {
                System.out.println("size chartsBySemester:" + chartsBySemester.size());
                if (chartsBySemester.size() == 0) {
                    request.setAttribute("msgErr", "Không có dữ liệu về điểm về kì học này");
                }
                else {
                    // phải tách ra 2 mảng, 1 mảng lưu Tên môn, 1 mảng lưu điểm trung bình. Từ đó mới xử lý trong JS
                    ArrayList<String> chartSubjectName = new ArrayList<>();
                    ArrayList<Float> chartAvgScore = new ArrayList<>();
                    for (Chart c: chartsBySemester){
                        chartSubjectName.add(c.getSubjectName());
                        chartAvgScore.add(c.getAvgScore());
                    }
                    request.setAttribute("chartSubjectName", chartSubjectName);
                    request.setAttribute("chartAvgScore", chartAvgScore);
                }
            } else {
                request.setAttribute("chartsBySemester", chartsBySemester);
                System.out.println("ss null");
            }
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
            dispatcher.forward(request, response);
        } catch (Exception e) {
            System.out.println("FollowBySemesterChart err:" + e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "/public/followBySemesterChart.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

}
