/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.SubjectDAO;
import DAO.YearDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Semester;
import model.Subject;
import model.Year;
import org.apache.jasper.tagplugins.jstl.ForEach;

/**
 *
 * @author NguyenDuc
 */
public class FollowBySemester extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "/public/followBySemester.jsp";
        try {
            request.setCharacterEncoding("UTF-8");
            response.setCharacterEncoding("UTF-8");
            response.setContentType("text/html; charset=UTF-8");
            HttpSession session = request.getSession();

            String semester = request.getParameter("semester");
            String year = request.getParameter("year");
            String optionSelected = request.getParameter("optionSelected");
            String studentCode = request.getParameter("studentCode");

            System.out.println(semester + "**" + year + "**" + optionSelected + "**" + studentCode);
            if (optionSelected.equals("Chọn cách xem")) {
                System.out.println("Chua chon cach xem");
                request.setAttribute("msgErr", "Chưa chọn cách xem");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
            }

            if (studentCode.equals("")) {
                System.out.println("Chua chon ma sv");
                request.setAttribute("msgErr", "Chưa nhập mã sinh viên");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
            }
            else {
                session.setAttribute("studentCode", studentCode.trim());
            }

            if (optionSelected.equals("Xem kì đã chọn") && semester.equals("Chọn kì học")) {
                System.out.println("Chua chon ki hoc");
                request.setAttribute("msgErr", "Chưa chọn kì học");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
            }

            int startYear = 0;
            int endYear = 0;
            if (optionSelected.equals("Xem kì đã chọn") && year.equals("Chọn năm học")) {
                System.out.println("Chua chon nam hoc");
                request.setAttribute("msgErr", "Chưa chọn năm học");
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
                dispatcher.forward(request, response);
            } else if (year.equals("Chọn năm học") == false) {
                request.setAttribute("msgErr", "");
                String[] yearSplit = year.split(" - ");
                startYear = Integer.parseInt(yearSplit[0]);
                endYear = Integer.parseInt(yearSplit[1]);
            }

            System.out.println("statYear:" + startYear);
            System.out.println("endYear:" + endYear);

            /*Truy vấn db trả về dữ liệu*/
            ArrayList<ArrayList<Subject>> semesterStatics = new ArrayList<>();
            SubjectDAO sd = new SubjectDAO();
            try {
                if (optionSelected.equals("Xem kì đã chọn")) {
                    System.out.println("Xem 1 ki");
                    ArrayList<Subject> record = sd.getScoreByStudentCode(semester, startYear, endYear, studentCode);
                    if (record != null && record.size() != 0) {
                        semesterStatics.add(record);
                    }
                } else {
                    System.out.println("Xem nhieu ki");
                    ArrayList<Semester> listSemester = sd.getListSemester();
                    for (Semester s : listSemester) {
                        ArrayList<Subject> record = sd.getScoreByStudentCode(s.getSemesterName(), s.getStartYear(), s.getEndYear(), studentCode);
                        if (record != null && record.size() != 0) {
                            semesterStatics.add(record);
                        }
                    }
                }
            } catch (Exception ex) {
                Logger.getLogger(FollowBySubject.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (semesterStatics != null) {
                request.setAttribute("semesterStatics", semesterStatics);
                System.out.println("size semesterStatics:" + semesterStatics.size());
                if (semesterStatics.size() == 0) {
                    request.setAttribute("msgErr", "Không có dữ liệu về điểm về kì học này");
                }
            } else {
                request.setAttribute("semesterStatics", semesterStatics);
                System.out.println("ss null");
            }
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
            dispatcher.forward(request, response);
        } catch (Exception e) {
            System.out.println("FollowBySemester err:" + e);
        }
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "/public/followBySemester.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }
}
