/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import model.User;
import DAO.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author NguyenDuc
 */
public class UserUpdate extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("_id");
        String fullName = request.getParameter("fullName");
        String dateOfBirth = request.getParameter("dateOfBirth");
        String email = request.getParameter("email");
        String phone = request.getParameter("phone");
        String userName = request.getParameter("userName");
        String role = request.getParameter("role");
        String nameSearch = request.getParameter("nameSearch");

        User user = new User();
        user.setId(Integer.parseInt(id));
        user.setFullName(fullName);
        user.setDateOfBirth(dateOfBirth);
        user.setEmail(email);
        user.setPhone(phone);
        user.setUserName(userName);
        user.setRole(role);

        UserDAO ud = new UserDAO();

        try {
            boolean checkUpdate = ud.updateInfoUser(user);
            if (checkUpdate){
                request.setAttribute("msgUpdate", "Cập nhật thông tin thành công");
                request.setAttribute("colorMsg", "green");
            }
            else{
                request.setAttribute("msgUpdate", "Cập nhật thông tin thất bại");
                request.setAttribute("colorMsg", "red");
            }

            ArrayList<User> users = ud.getInfoUser(nameSearch);
            if (users.size() > 0) {
                request.setAttribute("users", users);
                request.setAttribute("showTable", true);
            }
        } catch (Exception ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
        String url = "/public/user.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }
}
