/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.Excel;
import model.Subject;

/**
 *
 * @author NguyenDuc
 */
public class ExportScore extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            HttpSession httpSession = request.getSession(false);
            ArrayList<Subject> subjectScore = (ArrayList<Subject>) httpSession.getAttribute("subjectScore");
            System.out.println(subjectScore.size());

            Excel ex = new Excel();
            ServletContext sc = getServletContext();
//            String path = sc.getRealPath("export/bangdiemtp.xls");
            String path = "E:\\Đảm bảo chất lượng phần mềm\\btl_dbclpm\\web\\export\\bangdiemtp.xls";
            System.out.println("path:" + path);
            httpSession.setAttribute("pathDownload", path);
            ex.exportSubjectScoreToExcel(path, subjectScore);
            httpSession.setAttribute("showDownloadBtn", true);

            String url = "/public/followBySubject.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
            dispatcher.forward(request, response);
        } catch (Exception e) {
            System.out.println("ExportScore err:" + e.toString());
        }
    }
}
