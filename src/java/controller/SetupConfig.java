/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import DAO.ConfigDAO;
import DAO.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author NguyenDuc
 */
public class SetupConfig extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setCharacterEncoding("UTF-8");
        response.setContentType("text/html; charset=UTF-8");

        String tp1 = request.getParameter("tp1");
        String tp2 = request.getParameter("tp2");
        String tp3 = request.getParameter("tp3");
        String fi = request.getParameter("final");
        String subjectName = request.getParameter("subjectName");
        
        ConfigDAO cd = new ConfigDAO();

        try {
            boolean checkUpdate = cd.updateConfigSubject(tp1, tp2, tp3, fi, subjectName);
            if (checkUpdate) {
                request.setAttribute("msgUpdate", "Cập nhật cấu hình thành công");
                request.setAttribute("colorMsg", "green");
            } else {
                request.setAttribute("msgUpdate", "Cập nhật cấu hình thất bại");
                request.setAttribute("colorMsg", "red");
            }

            request.setAttribute("subjectName", subjectName);
            request.setAttribute("tp1", tp1);
            request.setAttribute("tp2", tp2);
            request.setAttribute("tp3", tp3);
            request.setAttribute("final", fi);

        } catch (Exception ex) {
            Logger.getLogger(model.User.class.getName()).log(Level.SEVERE, null, ex);
        }

        String url = "/public/setupConfig.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String url = "/public/setupConfig.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(url);
        dispatcher.forward(request, response);
    }
}
